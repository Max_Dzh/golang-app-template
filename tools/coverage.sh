#!/bin/bash
# удаляем предыдущие записи
rm cover/*.cov
# определяем степень покрытия кода тестами
PKG_LIST=$(go list ./... | grep -v /vendor/)
for package in ${PKG_LIST}; do
    echo "cover/${package##*/}.cov"
    go test -covermode=count -coverprofile "cover/${package##*/}.cov" "$package";
done
echo "mode: count" >> cover/coverage.cov
tail -q -n +2 cover/*.cov >> cover/coverage.cov
go tool cover -func=cover/coverage.cov
