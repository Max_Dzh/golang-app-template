# Project configuration
GIT_HOST?=gitlab.com

GIT_NAMESPACE?=Max_Dzh

REGISTRY_HOST?=registry.gitlab.com

APP?=golang-app-template

RELEASE?=1.0.0

COMMIT?=$(shell git rev-parse --short HEAD)

BUILD_TIME?=$(shell date -u '+%Y-%m-%d_%H:%M:%S')

BRANCH=$(shell git branch | grep \* | cut -d ' ' -f2)

PROJECT_DIR?=$(GIT_HOST)/$(GIT_NAMESPACE)/$(APP)

IMAGE?=$(REGISTRY_HOST)/$(GIT_NAMESPACE)/$(APP)/$(BRANCH):$(RELEASE)

GOOS?=linux

GOARCH?=amd64

# if exists
PORT?=1992

clean: ## clean old binary file
	rm -f bin/${APP}
	rm -f bin/main

build-local: clean ## building application's executable file
			CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} go build \
			-ldflags "-s -w -X ${PROJECT}/version.Release=${RELEASE} -X ${PROJECT}/version.AppName=${APP} \
			-X ${PROJECT}/version.Commit=${COMMIT} -X ${PROJECT}/version.BuildTime=${BUILD_TIME}" \
			-o bin/${APP}

build: dep clean  ## building application's executable file for the docker, but here, the name of the executable file should be - main
			CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} go build \
			-ldflags "-s -w -X ${PROJECT}/version.Release=${RELEASE}  -X ${PROJECT}/version.AppName=${APP} \
			-X ${PROJECT}/version.Commit=${COMMIT} -X ${PROJECT}/version.BuildTime=${BUILD_TIME}" \
			-o bin/main


run: build-local ## run app in a local environment
	PORT=${PORT} DOT=. bin/${APP}


image: build ## create docker image
	@docker build --build-arg PORT=${PORT} -t ${IMAGE} .


run-container: image ## run app as a docker container
		# останавливаем и удаляем предыдущий запущенный контейнер
	@docker stop ${APP}:${RELEASE} || true && docker rm ${APP}:${RELEASE} || true
		# флаг -v позволяет смонтировать папку в контейнер
		# в данном случае мы монтируем папку с секреткой в корневую папку контейнера
		# тоже самое происходит и при выкладке в кластер kubernetes
	@docker run --name ${APP} -p ${PORT}:${PORT} --rm  -v $(PWD)/app:/app \
		-e "PORT=${PORT}" \
		$(IMAGE)


manifest: ## creating a manifest from individual parts located in the kubernetes folder
	for t in kubernetes/*.yaml; do \
		cat $$t | \
        	sed -E "s/('|\"?)\IMAGE_NAME('|\"?)/$(REGISTRY_HOST)\/$(GIT_NAMESPACE)\/$(APP)\/$(BRANCH):$(RELEASE)/g" | \
        	sed -E "s/('|\"?)\SERVICE_NAME('|\"?)/$(APP)/g" | \
        	sed -E "s/('|\"?)\DEPLOYMENT_NAME('|\"?)/$(APP)-deployment/g" | \
        	sed -E "s/\APP_PORT/$(PORT)/g" | \
			sed -E "s/('|\")\APP_PORT('|\")/'$(PORT)'/g"; \
			echo "\n"---; \
			done > $(APP).yaml

deploy: ## deploy app to cluster
	@kubectl -n content delete deployment $(APP)-deployment
	@kubectl -n content create -f $(APP).yaml


push-image: image ## push app image to our gitlab registry
	@docker login $(REGISTRY_HOST)
	@docker push $(IMAGE)

push-ci-image: 
	@docker build -t  $(REGISTRY_HOST)/max_dzh/ci-container:1.0.0-light .
	@docker login $(REGISTRY_HOST)
	@docker push  $(REGISTRY_HOST)/max_dzh/ci-container:1.0.0-light



### gitlab CI

PKG_LIST := $(shell go list ${PROJECT_DIR}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: push-image

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unit tests
	@go test -short ${PKG_LIST}

race: dep # run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

dep: ## Get the dependencies
	@go get -v -d ./...

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'