package main

import "testing"

func TestGreeting(t *testing.T) {

	tests := []struct {
		Person         string
		MissingMessage string
	}{
		{
			"Guest",
			"Who are you?",
		},
		{
			"King",
			"Hello King",
		},
	}

	for _, tt := range tests {
		if greetingText := greeting(tt.Person); tt.MissingMessage != greetingText {
			t.Errorf("have %s, want %s", greetingText, tt.MissingMessage)
		}
	}
}
