FROM gcr.io/distroless/base

# получаем переменные сборки
# переданные через флаг --build-arg
ARG PORT

# при локальном запуске переменная содержит в себе символ .
# что позволяет прочитать учетные данные базы данных из локальной директории
ENV DOT $DOT

# указываем исполняемому файлу переменную порта
ENV PORT $PORT

# копируем исполняемый файл в рабочую директорию
WORKDIR /application
COPY bin/main .

CMD [ "./main" ]

