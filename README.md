[![coverage report](https://gitlab.com/Max_Dzh/golang-app-template/badges/master/coverage.svg)](https://gitlab.com/Max_Dzh/golang-app-template/commits/master) [![pipeline status](https://gitlab.com/Max_Dzh/golang-app-template/badges/master/pipeline.svg)](https://gitlab.com/Max_Dzh/golang-app-template/commits/master)

### Этот проект - шаблон для golang программ.
<hr>

#### В нем:

 - [x] Настроен gitlab CI.
 - [x] В пайплайнах настроены авто тесты, линтинг кода, проверка степени покрытия кода тестами и т.д.
 - [x] Если пайпланы успешно завершились, то происходит билд docker-образа приложения и его пуш в registry.
 - [x] Есть возможность пушить образ в registry через команду Makefile'a.
 - [x] При билде бинарника в переменные приложения кладутся актуальные данные о времени билда, последнем коммите и версии приложения, которые можно достать в процессе работы (например через какой-нибудь эндпоинт, если это API или вывести в логи). 

